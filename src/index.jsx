import React from 'react';
import ReactDOM from 'react-dom';
import { connect, Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';
import { applyMiddleware, createStore, compose } from 'redux';
import Element from './components/main/main.jsx';
import Element2 from './components/main2/main.jsx';

import Test from './components/loader/main.jsx';

const reducer = (state = {}, action) => {
	let opened, initiated;
	if (action.type === 'INIT') {
		initiated = true;
	}
	if (action.type === 'OPEN') {
		opened = true;
	}
	if (action.type === 'CLOSE') {
		opened = false;
	}
	return {
		...state,
		opened,
		initiated
		};
}

const open = () => {
	return { type: 'OPEN' };
}

const close = () => {
	return { type: 'CLOSE' };
}

const logger = createLogger();
const store = createStore(
  reducer,
	compose(
    applyMiddleware(thunk, promise, logger),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

const mapStateToProps = (state) => {
  return {
		inited: state.inited,
    opened: state.opened
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
		open: () => dispatch(open()),
		close: () => dispatch(close())
  }
}

const MegaButtons = React.createClass({
	render: function () {
		return (
			<div>
				<button onClick={this.props.open}>OPEN</button>
				<button onClick={this.props.close}>CLOSE</button>
			</div>
		);
	}
})

const App = React.createClass({
	render: function () {
		return <div>
			<Element {...this.props}/>
			<MegaButtons {...this.props}/>
		</div>
	}
})

const Container = connect(
	mapStateToProps,
	mapDispatchToProps
)(App);

const routes = (
	<Router hsitory={browserHistory}>
		<Route path="/" component={Container} />
		<Route path="/test" component={Test} />
	</Router>
);

store.dispatch({type:'INIT'})

ReactDOM.render(
	<Provider store={store}>
		{routes}
	</Provider>,
	document.getElementById('app')
);
