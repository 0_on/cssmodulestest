import React from 'react';

export default (props) => (
	<ul>
		{props.names.map(e => (
			<li>{e}</li>
		))}
	</ul>
)