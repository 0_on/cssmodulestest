import React from 'react';
import Comp from './text.jsx';

const names1 = ['123', '456'];
const names2 = ['qwe', 'rty'];

export default () => (
	<div>
		<Comp names={names1} />
		<Comp names={names2} />
	</div>
)