import React from 'react';
import { Link } from 'react-router';

import styles from './style.css';

const context = require.context('babel!../', true, /test.jsx$/);

const modules = context.keys().map(path => ({
	path: path.substr(1), 
	module: context(path).default
}));


const parseUrl = url => url.replace('?url=', '');

const getModule = url => {
	const module = modules.find(e => e.path.indexOf(parseUrl(url)) >= 0);
	return module ? 
		module.module : 
		() => <div>Module not found :(</div>;
}

export default (props) => (
	<div>
		<div className={styles.menu}>
			<ul>
				{modules.map(e => (
					<li>
						<a href={`#/test?url=${e.path}`}>{e.path}</a>
					</li>
				))}
			</ul>
		</div>
		<div className={styles.container}>
			{React.createElement(getModule(props.location.search), {}, '')}
		</div>
	</div>
)
