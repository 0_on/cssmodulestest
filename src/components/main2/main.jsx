import React from 'react';
import styles from './main.css';

export default React.createClass({
	render: function () {
		return (
			<div className={styles.root}>
				<div className={styles.pic}/>
				<div className={styles.info}>
					<span className={styles.name}>Aeiuf</span>
					<span className={styles.email}>aeiuf@sefklm.rdt</span>
				</div>
				<div className={styles.menupart}>
					<a className={styles.link} href="#">awdawdawdaw</a>
					<div className={this.props.opened ? styles.dropdownOpen : styles.dropdown }>
						<ul>
							<li>punkt 1</li>
							<li>punkt 2</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
})
